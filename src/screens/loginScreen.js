import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
  ToastAndroid,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import auth from '@react-native-firebase/auth';
import {connect} from 'react-redux';

class loginScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      check_textInputChange: false,
      password: '',
      email: '',
      secureTextEntry: true,
    };
  }
  textInputChange(value) {
    if (value.length !== 0) {
      this.setState({
        check_textInputChange: true,
      });
    } else {
      this.setState({
        check_textInputChange: false,
      });
    }
  }
  secureTextEntry() {
    this.setState({
      secureTextEntry: !this.state.secureTextEntry,
    });
  }

  toast(msg) {
    ToastAndroid.showWithGravity(msg, ToastAndroid.SHORT, ToastAndroid.CENTER);
  }

  handleLogIn = () => {
    const {email, password} = this.state;
    const users = this.props.users.filter((item) => item.email === email);
    // auth()
    //   .signInWithEmailAndPassword(this.state.email, this.state.password)
    //   .then(() => {
    //     console.log('User account created & signed in!');
    //   });
    try {
      if (users[0].email === email) {
        if (users[0].password === password) {
          this.toast('Yeay, Login Berhasil!');
          this.props.setLogin(true);
          this.props.navigation.replace('homeScreen');
          console.log(this.props.isLogin);
        } else {
          this.toast('Password Masih Salah, Nih');
        }
      } else {
        this.toast('Email Kamu Salah');
      }
    } catch (error) {
      this.toast('Pastiin Udah Isi Email dan Password Ya!');
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Image
            style={{width: 150, height: 150}}
            source={require('../assets/MHD.png')}
          />
        </View>
        <View style={styles.footer}>
          <Text style={{fontSize: 38, fontWeight: 'bold', color: '#272727'}}>
            Log in
          </Text>
          <Text style={{marginTop: 3, marginBottom: 20, color: '#272727'}}>
            Login to continue
          </Text>
          <View style={{marginBottom: 19}}>
            <Text
              style={{
                marginBottom: 10,
                color: '#272727',
                fontWeight: 'bold',
                fontSize: 13.5,
              }}>
              Email
            </Text>
            <TextInput
              placeholder="abc@email.com"
              style={styles.TextInput}
              onChangeText={(text) =>
                this.setState({
                  email: text,
                })
              }
            />
          </View>
          <View style={{marginBottom: 30}}>
            <Text
              style={{
                marginBottom: 10,
                color: '#272727',
                fontWeight: 'bold',
                fontSize: 13.5,
              }}>
              Password
            </Text>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              {this.state.secureTextEntry ? (
                <TextInput
                  placeholder="xxxxffffffffffffffff"
                  secureTextEntry={true}
                  value={this.state.password}
                  style={{
                    backgroundColor: '#F2F2F6',
                    paddingLeft: 10,
                    height: 50,
                    borderRadius: 10,
                    flex: 8,
                  }}
                  onChangeText={(text) =>
                    this.setState({
                      password: text,
                    })
                  }
                />
              ) : (
                <TextInput
                  placeholder="xxxxffffffffffffffff"
                  value={this.state.password}
                  style={{
                    backgroundColor: '#F2F2F6',
                    paddingLeft: 10,
                    height: 50,
                    borderRadius: 10,
                    flex: 8,
                  }}
                  onChangeText={(text) =>
                    this.setState({
                      password: text,
                    })
                  }
                />
              )}
              <TouchableOpacity onPress={() => this.secureTextEntry()}>
                {this.state.secureTextEntry ? (
                  <Icon
                    name="eye-slash"
                    size={25}
                    color="#B6B6B6"
                    style={{marginLeft: 10, flex: 1}}
                  />
                ) : (
                  <Icon
                    name="eye"
                    size={25}
                    color="#B6B6B6"
                    style={{marginLeft: 10, flex: 1}}
                  />
                )}
              </TouchableOpacity>
            </View>
          </View>
          <View>
            <TouchableOpacity style={styles.button}>
              <Text
                style={{
                  color: '#FEFEFE',
                  fontSize: 20,
                  textAlign: 'center',
                  fontWeight: 'bold',
                }}
                onPress={() => this.handleLogIn()}>
                {/* //   auth()
                //     .signInWithEmailAndPassword(
                //       this.state.email,
                //       this.state.password,
                //     )
                //     .then(() => {
                //       console.log('User account created & signed in!');
                //     });
                // }}> */}
                Log me in!
              </Text>
            </TouchableOpacity>
          </View>
          <View>
            <TouchableOpacity
              style={styles.buttonbawah}
              onPress={() => this.props.navigation.navigate('registerScreen')}>
              <Text
                style={{
                  color: '#272727',
                  fontSize: 20,
                  textAlign: 'center',
                  fontWeight: 'bold',
                }}>
                New user?
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    users: state.users,
    pass: state.password,
    email: state.email,
    name: state.name,
    isLogin: state.isLogin,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setLogin: (isLogin) => {
      dispatch({
        type: 'LOGIN',
        payload: isLogin,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(loginScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FDE74C',
  },
  header: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FDE74C',
  },
  footer: {
    flex: 2,
    backgroundColor: '#FEFEFE',
    borderTopLeftRadius: 35,
    borderTopRightRadius: 35,
    paddingTop: 26,
    paddingLeft: 22,
    paddingBottom: 17,
    paddingRight: 23,
  },
  TextInput: {
    backgroundColor: '#F2F2F6',
    paddingLeft: 10,
    height: 50,
    borderRadius: 10,
    justifyContent: 'center',
  },
  button: {
    backgroundColor: '#D01774',
    height: 60,
    justifyContent: 'center',
    borderRadius: 10,
    marginBottom: 10,
  },
  buttonbawah: {
    backgroundColor: '#FEFEFE',
    height: 60,
    justifyContent: 'center',
    borderRadius: 10,
    marginBottom: 10,
    borderColor: '#B6B6B6',
    borderWidth: 1.5,
  },
});

import React, {Component} from 'react';
import {StyleSheet, View, Text, TouchableOpacity, Image} from 'react-native';
import {connect} from 'react-redux';

class splashScreen extends Component {
  componentDidMount() {
    setTimeout(() => {
      if (this.props.isLogin) {
        this.props.navigation.replace('homeScreen');
      } else {
        this.props.navigation.replace('loginScreen');
      }
    }, 3000);
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={{alignItems: 'center', justifyContent: 'center'}}>
          <Image
            style={{width: 144, height: 144}}
            source={require('../assets/MHD.png')}
          />
          <Text
            style={{
              marginTop: 1,
              fontWeight: 'bold',
              fontSize: 37,
              color: '#26398C',
            }}>
            SALT ACADEMY
          </Text>
          <Text
            style={{
              marginTop: 4,
              fontWeight: 'normal',
              fontSize: 12,
              color: '#26398C',
            }}>
            REACT-NATIVE MOBILE HYBRID DEVELOPMENT
          </Text>
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isLogin: state.isLogin,
  };
};

export default connect(mapStateToProps)(splashScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FDE74C',
  },
});

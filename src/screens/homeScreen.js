import React, {Component} from 'react';
import {StyleSheet, View, Text, TouchableOpacity, Image} from 'react-native';
import {connect} from 'react-redux';

class homeScreen extends Component {
  handleLogOut = () => {
    this.props.setLogin(false);
    this.props.navigation.replace('loginScreen');
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={{alignItems: 'center', justifyContent: 'center'}}>
          <Image
            style={{width: 144, height: 144}}
            source={require('../assets/MHD.png')}
          />
          <Text
            style={{
              marginTop: 1,
              fontWeight: 'bold',
              fontSize: 37,
              color: '#26398C',
            }}>
            SALT ACADEMY
          </Text>
          <Text
            style={{
              marginTop: 4,
              fontWeight: 'normal',
              fontSize: 12,
              color: '#26398C',
            }}>
            Hello, there!
          </Text>
        </View>
        <View>
          <TouchableOpacity style={styles.button}>
            <Text
              style={{
                color: '#FEFEFE',
                fontSize: 20,
                textAlign: 'center',
                fontWeight: 'bold',
              }}
              onPress={() => this.handleLogOut()}>
              Log Out
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    users: state.users,
    isLogin: state.isLogin,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setLogin: (isLogin) => {
      dispatch({
        type: 'LOGOUT',
        payload: isLogin,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(homeScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FDE74C',
  },
  button: {
    backgroundColor: '#D01774',
    height: 50,
    justifyContent: 'center',
    borderRadius: 10,
    marginBottom: 10,
    marginTop: 10,
  },
});

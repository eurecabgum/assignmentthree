const initialState = {
  name: '',
  password: '',
  email: '',
  users: [
    {name: 'Eca', password: 'tes123', email: 'eca@gmail.com'},
    {name: 'Akmal', password: 'tas123', email: 'akmal@gmail.com'},
    {name: 'Sukron', password: 'tos123', email: 'sukron@gmail.com'},
  ],
  inputform: {},
  isLogin: false,
};

const userReducer = (state = initialState, Action) => {
  switch (Action.type) {
    case 'ADD-DATA':
      return {
        ...state,
        users: [...state.users, Action.payload],
      };
    case 'LOGIN':
      return {
        ...state,
        isLogin: Action.payload,
      };
    case 'LOGOUT':
      return {
        ...state,
        isLogin: Action.payload,
      };
    default:
      return state;
  }
};

export default userReducer;

import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import splashScreen from '../screens/splashScreen';
import loginScreen from '../screens/loginScreen';
import registerScreen from '../screens/registerScreen';
import homeScreen from '../screens/homeScreen';
const Stack = createStackNavigator();

const StackNavigator = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="splashScreen"
        component={splashScreen}
        options={{
          title: 'SPLASH SCREEN',
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="loginScreen"
        component={loginScreen}
        headerShown={false}
        options={{
          title: 'LOGIN SCREEN',
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="registerScreen"
        component={registerScreen}
        headerShown={false}
        options={{
          title: 'REGISTER SCREEN',
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="homeScreen"
        component={homeScreen}
        headerShown={false}
        options={{
          title: 'HOME SCREEN',
          headerShown: false,
        }}
      />
    </Stack.Navigator>
  );
};

const Router = () => {
  return (
    <NavigationContainer>
      <StackNavigator />
    </NavigationContainer>
  );
};

export default Router;

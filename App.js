import React, {Component} from 'react';
import {StyleSheet, View, Text} from 'react-native';
import {Store, Persistor} from './src/redux/store';
import Router from './src/router/index';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';

class App extends Component {
  render() {
    return (
      <Provider store={Store}>
        <PersistGate loading={null} persistor={Persistor}>
          <Router />
        </PersistGate>
      </Provider>
    );
  }
}

export default App;

const styles = StyleSheet.create({});
